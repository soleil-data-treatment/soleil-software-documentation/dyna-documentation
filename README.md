# Aide et ressources de DYNA pour Synchrotron SOLEIL

[<img src="https://software.pan-data.eu/cache/b/9/4/a/d/b94ad8411743a9c1864b0bb7fe28768acaa09a9b.jpeg" width="150"/>](http://dyna.neel.cnrs.fr/)

## Résumé

- traitement réflectivité pour des matériaux magnétiques, calcul de réflectivité, prend en compte la réflexion ascendante du faisceau 
- Open source

## Sources

- Code source:  http://dyna.neel.cnrs.fr/index.php/downloads/
- Documentation officielle: http://dyna.neel.cnrs.fr/index.php/documents/

## Navigation rapide

| Fichiers téléchargeables | Tutoriaux | Ressources annexes | Page pan-data |
| - | - | - | - |
| [Documents d'utilisation](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/dyna-documentation/-/tree/master/documents) | [Tutoriel d'installation officiel](http://dyna.neel.cnrs.fr/index.php/downloads/) | [Publications utilisant DYNA](http://neel-2007-2019.neel.cnrs.fr/spip.php?rubrique1008) | [Documentation pan-data du logiciel](https://software.pan-data.eu/software/174/dyna) |
|   | [Tutoriaux officiels](https://gitlab.com/soleil-data-treatment/soleil-software-documentation/dyna-documentation/-/blob/master/documents/Tuto_DYNA_XRMR_2018-09.pdf)  |  |  |

## Installation

- Systèmes d'exploitation supportés: Windows,  MacOS,  matlab
- Installation: Facile (tout se passe bien)

## Format de données

- en entrée: .txt
- en sortie: empilements de couches,  composition indice optique,  fichier colbatère
- sur un disque dur,  sur la Ruche locale

