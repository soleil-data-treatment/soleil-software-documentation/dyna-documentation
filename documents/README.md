| Document | Description |
| ------ | ------ |
| Tuto_DYNA_XRMR_2018-09.pdf | User manual for DYNA by Emmanuelle Jal - 2018 |
| Magnetic_Reflectivity.pdf | Eigenwave formalism andapplication to a W/Fe/W trilayer by Marta Elzo, Emmanuelle Jal, Oana Bunau, Stéphane Grenier, Aline Y.Ramos, Hélio Tolentino, Yves Joly, Jean-Marc Tonnerre, Nicolas Jaouen |

